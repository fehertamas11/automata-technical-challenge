#include <check.h>               
#include <stdlib.h>
#include "../MAX7317.h"


START_TEST(test_MAX7317_init) {
    MAX7317_RETURN_T retval = MAX7317_init();
   ck_assert_uint_eq(retval,MAX7317_RETURN_OK);
} END_TEST


START_TEST(test_MAX7317_readPortConfig) {
    MAX7317_CONF_T config;
    for(uint8_t portIterator=0;portIterator<10;++portIterator){

        MAX7317_RETURN_T retval = MAX7317_readPortConfig(portIterator,&config);
        ck_assert_uint_eq(retval,MAX7317_RETURN_OK);
    }
} END_TEST

START_TEST(test_MAX7317_setPortConfig) {
        MAX7317_CONF_T config=MAX7317_INPUT;
        for(uint8_t portIterator=0;portIterator<10;++portIterator){

            MAX7317_RETURN_T retval = MAX7317_setPortConfig(portIterator,config);
            ck_assert_uint_eq(retval,MAX7317_RETURN_OK);
        }
} END_TEST


START_TEST(test_MAX7317_readAllInputPort) {
    uint16_t levels=0xFF;
    MAX7317_RETURN_T retval = MAX7317_readAllInputPort(&levels);
   ck_assert_uint_eq(retval,MAX7317_RETURN_OK);
} END_TEST


START_TEST(test_MAX7317_setAllPortConfig) {
    MAX7317_CONF_T config=MAX7317_INPUT;
    MAX7317_RETURN_T retval = MAX7317_setAllPortConfig(config);
   ck_assert_uint_eq(retval,MAX7317_RETURN_OK);
} END_TEST


Suite *max7317_suite(void) {
  Suite *s;
  TCase *tc_core;

  s = suite_create("max7317");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_MAX7317_init);
  tcase_add_test(tc_core, test_MAX7317_readPortConfig);
  tcase_add_test(tc_core, test_MAX7317_setPortConfig);
  tcase_add_test(tc_core, test_MAX7317_readAllInputPort);
  tcase_add_test(tc_core, test_MAX7317_setAllPortConfig);
  suite_add_tcase(s, tc_core);

  return s;
}

int main(void) {
  int no_failed = 0;                   
  Suite *s;                            
  SRunner *runner;                     

  s = max7317_suite();
  runner = srunner_create(s);          

  srunner_run_all(runner, CK_NORMAL);  
  no_failed = srunner_ntests_failed(runner); 
  srunner_free(runner);                      
  return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;  
}
