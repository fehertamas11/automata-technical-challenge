## Notes about the challenge
+ Assuming that the SPI library releases the CS pin after a read or a write for at least one SPI clock cycle
+ Little endian transmission in required
+ First I implemented the read function poorly, for not reading the correct registers and not issuing a second read/write as described in the datasheet page 8 step 6.
+ This was a genuinely fun challenge with the provided library and the well specified tasks.
## Testing
I wrote a simple test environment, which checks if the functions return correctly after addressing each port. 
The test is using the Check library, and can be compiled and executed by running the `test.sh` script.

The solution was tested on an Ubuntu 18.04 machine.
