#ifndef MAX7317_H_
#define MAX7317_H_

#include <stdint.h>
#include "lib/automata_spi.h"

/**
 * Describes the port output configuration
 */
typedef enum {
    MAX7317_OPEN_DRAIN = 0,         /* Open drain mode */
    MAX7317_INPUT = 1               /* High impedance mode */
} MAX7317_CONF_T;

/**
 * Describes the logic voltage level of an input port
 */
typedef enum{
    MAX7317_LEVEL_LOW=0,            /* Logic low voltage  level */
    MAX7317_LEVEL_HIGH=1            /* Logic high voltage level */
}MAX7317_LEVEL_T;

/**
 * Return values of the MAX7317 functions
 */
typedef enum{
    MAX7317_RETURN_OK = 0,          /* succesful return */
    MAX7317_RETURN_INVALID_ARG,     /* Argument contains out of range value */
    MAX7317_RETURN_NULLPTR,         /* Argument contains null pointer */
    MAX7317_RETURN_SPI_FAILURE      /* SPI error during the operation */
} MAX7317_RETURN_T;

/**
 * @brief Configures the SPI communication for the data transmission. This needs to be
 * called only once before communicating with the chip
 * @return Error description
 */
MAX7317_RETURN_T MAX7317_init();

/**
 * @brief Configures a port to either high impedance imput, or open drain output
 * @param port The selected port to configure. range: 0-9
 * @param config The configuration to set the port
 * @return Error description
 */
MAX7317_RETURN_T MAX7317_setPortConfig(const uint8_t port, const MAX7317_CONF_T config);

/**
 * @brief Reads the configuration of a single port
 * @param port Specifies which port to read. Range: 0-9
 * @param config The current configuration is written into this pointer.
 * @return Error description
 */
MAX7317_RETURN_T MAX7317_readPortConfig(const uint8_t port, MAX7317_CONF_T * config );

/**
 * @brief Cofigures all of the ports to either high impedance imput, or open drain output.
 * @param config The configuration to write all the ports.
 * @return Error description
 */
MAX7317_RETURN_T MAX7317_setAllPortConfig(const MAX7317_CONF_T config);

/**
 * @brief Reads the input voltage level of a port.
 * @param port The port to read from
 * @param level the pointer to write the level into.
 * @return Error description
 */
MAX7317_RETURN_T MAX7317_readInputPort(uint8_t port, MAX7317_LEVEL_T * level );

/**
 * @brief Reads the input voltage level of all the ports.
 * @param levels the pointer to write the levels into. b0 to b9 represents each port,
 * where 0 means logic low and 1 means logic high. Bits' b10 to b15 state is undefined.
 * @return Error description
 */
MAX7317_RETURN_T MAX7317_readAllInputPort(uint16_t * levels );
#endif