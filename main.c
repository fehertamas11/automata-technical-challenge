#include "MAX7317.h"
#include <stdio.h>
#include <stdlib.h>
#include <check.h>

int main(void)
{
	MAX7317_CONF_T config=MAX7317_OPEN_DRAIN;
	uint8_t port=4;
	MAX7317_LEVEL_T input_level;
	uint16_t ImportPortLevels;

	/*Initializing hardware */
    MAX7317_init();

    /* configuring port */
	MAX7317_setPortConfig(port,config);

	/* reading port configuration */
	MAX7317_readPortConfig(port, &config);

	/* configuring all the ports as input*/
	MAX7317_setAllPortConfig(MAX7317_INPUT);

	/* reading port input logic level */
	MAX7317_readInputPort(port,&input_level);

	/* reading all input logic levels */
	MAX7317_readAllInputPort(&ImportPortLevels);
}
