#include "MAX7317.h"
#include <stdlib.h>

/*Constant variables */

static const uint32_t max7317_baudrate=26000000; /* Acconding to the datasheet, 26Mbps is a safe speed to use */

static const uint32_t max7317_cs_to_sck=10; /* Minimum time between asserting CS & SCK edge */
static const uint32_t max7317_sck_to_cs=3;  /* Minimum time between SCK edge & releasing CS */
static const uint8_t max7317_port_count=10; /* Number of ports provided by the chip */
/* Register definitions */
static const uint8_t max7317_reg_write_all=0x0A;        /* Register to write the configuration in all ports */
static const uint8_t max7317_reg_read_p7_to_p0=0x0E;    /* Register which contains the input level of p7 to p0 */
static const uint8_t max7317_reg_read_p9_to_p8=0x0F;    /* Register which contains the input level of p9 to p8 */
static const uint8_t max7317_reg_no_operation=0x20;     /* Register which does no operation when written */

/* Function prototypes */

/**
 * @brief Reads a register from the chip using SPI
 * @param reg_addr The address of the register to read from
 * @param value pointer to the read byte
 * @return Error description
 */
MAX7317_RETURN_T MAX7317_readRegister( const uint8_t reg_addr, uint8_t *value);

/**
 * @brief Writes a register of the chip using SPI
 * @param reg_addr The address of the register to write into
 * @param value The byte that's going to be written in the register
 * @return Error description
 */
MAX7317_RETURN_T MAX7317_writeRegister( const uint8_t reg_addr, const uint8_t value);

/* Function definitions */

MAX7317_RETURN_T MAX7317_init() {
    /* Configure the SPI channel */
    SPI_CONFIG_T max7317_spi_config;
    /* most of the configuration is sourced from page 4. of the datasheet */
    max7317_spi_config.cs_pol=SPI_ACTIVE_LOW;
    max7317_spi_config.cpol=SPI_IDLE_LOW;
    max7317_spi_config.cpha=SPI_EDGE_RISING;
    max7317_spi_config.bits_per_frame=16;
    max7317_spi_config.baudrate=max7317_baudrate;
    max7317_spi_config.cs_to_sck=max7317_cs_to_sck;
    max7317_spi_config.sck_to_cs=max7317_sck_to_cs;
    /* Configuring the SPI and checking for errors */
    if(SPI_configureTransfer(&max7317_spi_config)!=SPI_RETURN_ok){
        return MAX7317_RETURN_SPI_FAILURE;
    }
    return MAX7317_RETURN_OK;

}

MAX7317_RETURN_T MAX7317_readRegister(const uint8_t reg_addr, uint8_t *value){
    SPI_TRANSFER_T spi_buffer;
    /* setting up the transfer bytes during the initialization */
    /* the last byte of the register address is set to 1 to indicate a read command */
    /* The second transmit byte here is just dummy data */
    uint8_t transmit_buffer[2]={reg_addr| 0x01 << 7, 0};
    uint8_t receive_buffer[sizeof(transmit_buffer)];
    spi_buffer.tx_data=transmit_buffer;
    spi_buffer.rx_data=receive_buffer;
    spi_buffer.number_of_bytes=sizeof(transmit_buffer);
    /* transmitting 2 bytes */
    if(SPI_RETURN_ok != SPI_transferBlocking(&spi_buffer)){ /*checking for SPI error */
        return MAX7317_RETURN_SPI_FAILURE;
    }
    /* According to the datasheet page 8, a second command needs to be issued,
     * in order to receive the requested byte. the byte will be in the 2 byte of the register*/
    spi_buffer.tx_data[0]=max7317_reg_no_operation;
    if(SPI_RETURN_ok != SPI_transferBlocking(&spi_buffer)){ /*checking for SPI error */
        return MAX7317_RETURN_SPI_FAILURE;
    }
    *value=spi_buffer.rx_data[1];
    return MAX7317_RETURN_OK;
}

MAX7317_RETURN_T MAX7317_writeRegister( const uint8_t reg_addr, const uint8_t value){
    SPI_TRANSFER_T spi_buffer;

    /* setting up the transfer bytes during the initialization */
    /* the last byte of the register address is set to 0 to indicate a write command */
    /* The second byte here is set in the register */
    uint8_t transmit_buffer[2]={reg_addr& ~(0x01 << 7), value};
    uint8_t receive_buffer[sizeof(transmit_buffer)];

    spi_buffer.tx_data=transmit_buffer;
    spi_buffer.rx_data=receive_buffer;
    spi_buffer.number_of_bytes=sizeof(transmit_buffer);

    /*Transmitting two bytes, rx data is discarded */
    if(SPI_RETURN_ok != SPI_transferBlocking(&spi_buffer)){ /*checking for SPI error */
        return MAX7317_RETURN_SPI_FAILURE;
    }
    return MAX7317_RETURN_OK;
}

MAX7317_RETURN_T MAX7317_setPortConfig(const uint8_t port, const MAX7317_CONF_T config){
    /* Checking input arguments */
    if(port>=max7317_port_count || config>1){
        return MAX7317_RETURN_INVALID_ARG;
    }

    /*Writing to the port numbered register and returning with it's error code */
    return MAX7317_writeRegister(port,config);

}

MAX7317_RETURN_T MAX7317_readPortConfig(const uint8_t port, MAX7317_CONF_T * config ){
    uint8_t config_reg;
    /* Checking the input arguments */
    if(port>=max7317_port_count){
        return MAX7317_RETURN_INVALID_ARG;
    }
    if(NULL==config){
        return MAX7317_RETURN_NULLPTR;
    }

    /* Reading the port numbered register */
    if( MAX7317_RETURN_OK!=MAX7317_readRegister(port,&config_reg)){ /* checking for SPI failure */
        return MAX7317_RETURN_SPI_FAILURE;
    }

    /* returning with high impedance if the register is not 0 */
    if(config_reg){
        *config=MAX7317_INPUT;
    }else{
        *config=MAX7317_OPEN_DRAIN;
    }
    return MAX7317_RETURN_OK;
}

MAX7317_RETURN_T MAX7317_setAllPortConfig(const MAX7317_CONF_T config){
    /* Checking the input arguments */
    if(config>1){
        return MAX7317_RETURN_INVALID_ARG;
    }

    /*Writing to the register of all the ports and returning with it's error code */
    return MAX7317_writeRegister(max7317_reg_write_all,config);
}

MAX7317_RETURN_T MAX7317_readInputPort(uint8_t port, MAX7317_LEVEL_T * level ){
    uint8_t reg_to_read;
    uint8_t reg_value;

    /* Checking the input arguments */
    if(NULL==level){
        return MAX7317_RETURN_NULLPTR;
    }

    /* the port input levels can't be addressed individually, so a container register needs reading,
     * and selecting the proper bit of that */
    /* if the user wants to read a higher port than p7, another register is read. */
    if(port>7){
        /* only check whether the input port is too big, when it's sure that its bigger than p7*/
        if(port>=max7317_port_count){
            return MAX7317_RETURN_INVALID_ARG;
        }
        /* 8 is substracted from the port to make sure that the bit operations are correct */
        port-=8;
        reg_to_read= max7317_reg_read_p9_to_p8;
    }else{

        reg_to_read= max7317_reg_read_p7_to_p0;
    }

    /*Reading the specified register and checking for errors */
    if(MAX7317_RETURN_OK != MAX7317_readRegister(reg_to_read,&reg_value) ){ /* checking for SPI failure */
        return MAX7317_RETURN_SPI_FAILURE;
    }
    /* checking the right bit of the return register for the output */
    if(reg_value & (1<<port)){
        *level=MAX7317_LEVEL_HIGH;
    }else{
        *level=MAX7317_LEVEL_LOW;
    }
    return MAX7317_RETURN_OK;
}

MAX7317_RETURN_T MAX7317_readAllInputPort(uint16_t * levels ){
    uint8_t reg_value;

    /* Checking the input arguments */
    if(NULL==levels){
        return MAX7317_RETURN_NULLPTR;
    }

    /* reading both of the input level registers */
    if(MAX7317_RETURN_OK != MAX7317_readRegister(max7317_reg_read_p9_to_p8,&reg_value) ){ /* checking for SPI failure */
        return MAX7317_RETURN_SPI_FAILURE;
    }

    /*shifting the result 1 byte to the left and writing it to the output */
    *levels=reg_value<<8;
    if(MAX7317_RETURN_OK != MAX7317_readRegister(max7317_reg_read_p7_to_p0,&reg_value) ){ /* checking for SPI failure */
        return MAX7317_RETURN_SPI_FAILURE;
    }

    /* adding the second register to the least significant byte */
    *levels |=reg_value;
    return MAX7317_RETURN_OK;

}
